#!/bin/bash

version=$2
rmfile=$3

# first lets remove the file we downloaded (it's not really the
# package, just an index file from svn
rm -f $rmfile

echo "Exporting processing-core from svn"

# now lets checkout the code
address=http://processing.googlecode.com/svn/tags/processing-${version}/core
svn -q export $address processing-core-${version}

# remove the methods stuff - the source for the jar has no license
# header so we can't ship this component. It's not really needed
# anyway since it only provides an ant method used in the build.
rm -rf processing-core-${version}/methods/

# the dest file should be the same place uscan downloaded to
outdir=`dirname $rmfile`

tar -czf $outdir/processing-core_${version}.orig.tar.gz processing-core-${version}

# cleanup
rm -rf processing-core-${version}

